
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for astra_instance
-- ----------------------------
DROP TABLE IF EXISTS `astra_instance`;
CREATE TABLE `astra_instance` (
`astra_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`control_server_addr`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`event_request`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`astra_mask`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`auth_api`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
PRIMARY KEY (`astra_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for channel
-- ----------------------------
DROP TABLE IF EXISTS `channel`;
CREATE TABLE `channel` (
`channel_id`  int(11) UNSIGNED NOT NULL ,
`astra_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`name_rus`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`lumi_fr`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`type`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`enable`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'true' ,
`event`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'true' ,
`current_input_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`channel_pnr`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`stalker_on`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`lumi_on`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`stalker_on2`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`keys_shar`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
PRIMARY KEY (`channel_id`),
FOREIGN KEY (`astra_id`) REFERENCES `astra_instance` (`astra_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
INDEX `channel_astra_fk` (`astra_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Table structure for dvb_input
-- ----------------------------
DROP TABLE IF EXISTS `dvb_input`;
CREATE TABLE `dvb_input` (
`dvb_input_id`  varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`type`  enum('S','S2','T','T2','C','ASI') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'S' ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`adapter`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`device`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`mac`  varchar(17) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`budget`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`buffer_size`  int(11) UNSIGNED NOT NULL DEFAULT 2 ,
`modulation`  enum('NONE','QPSK','QAM16','QAM32','QAM64','QAM128','QAM256','AUTO','VSB8','VSB16','PSK8','APSK16','APSK32','DQPSK','QPSK','QAM16','QAM32','QAM64','QAM128','QAM256','AUTO','VSB8','VSB16','PSK8','APSK16','APSK32','DQPSK') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`fec`  enum('NONE','1/2','2/3','3/4','4/5','5/6','6/7','7/8','8/9','AUTO','3/5','9/10') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`frequency`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`polarization`  enum('V','H','R','L') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'V' COMMENT 'S/S2' ,
`symbolrate`  int(11) UNSIGNED NOT NULL DEFAULT 27500 COMMENT 'S/S2' ,
`lof1`  int(11) UNSIGNED NOT NULL DEFAULT 10750 COMMENT 'S/S2' ,
`lof2`  int(11) UNSIGNED NOT NULL DEFAULT 10750 COMMENT 'S/S2' ,
`slof`  int(11) UNSIGNED NOT NULL DEFAULT 10750 COMMENT 'S/S2' ,
`lnb_sharing`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' COMMENT 'S/S2' ,
`alert_status_dvb`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`diseqc`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'S/S2' ,
`tone`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' COMMENT 'S/S2' ,
`rolloff`  enum('35','20','25','AUTO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'S2' ,
`bandwidth`  enum('8mhz','7mhz','6mhz','AUTO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'T/T2' ,
`guardinterval`  enum('1/32','1/16','1/8','1/4','AUTO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'T/T2' ,
`transmitmode`  enum('2K','8K','AUTO','4K') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'T/T2' ,
`hierarchy`  enum('NONE','1','2','4','AUTO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'T/T2' ,
`no_sdt`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`no_eit`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`pass_sdt`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`pass_eit`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`stream_id`  int(11) UNSIGNED NULL DEFAULT NULL COMMENT 'T2' ,
`last_update`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'monitoring' ,
`status`  int(11) NOT NULL DEFAULT 0 COMMENT 'monitoring' ,
`signal`  int(11) NOT NULL DEFAULT 0 COMMENT 'monitoring' ,
`snr`  int(11) NOT NULL DEFAULT 0 COMMENT 'monitoring' ,
`ber`  int(11) NOT NULL DEFAULT 0 COMMENT 'monitoring' ,
`unc`  int(11) NOT NULL DEFAULT 0 COMMENT 'monitoring' ,
`event`  enum('false','true') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'true' ,
`last_problem_dvb`  timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ,
PRIMARY KEY (`dvb_input_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Table structure for input
-- ----------------------------
DROP TABLE IF EXISTS `input`;
CREATE TABLE `input` (
`channel_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`input_0_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`input_0`  varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`onair`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`bitrate`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`scrambled`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`alert_status`  enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' ,
`cc_error`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`pes_error`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`last_update`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`last_problem`  timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ,
PRIMARY KEY (`channel_id`),
FOREIGN KEY (`channel_id`) REFERENCES `channel` (`channel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `channel_newcamd_fk` (`channel_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Table structure for output
-- ----------------------------
DROP TABLE IF EXISTS `output`;
CREATE TABLE `output` (
`channel_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`output_0_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 ,
`output_0`  varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
PRIMARY KEY (`channel_id`),
FOREIGN KEY (`channel_id`) REFERENCES `channel` (`channel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `output_channel_fk` (`channel_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
`user_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`user`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`password`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
PRIMARY KEY (`user_id`),
UNIQUE INDEX `user_password` (`user`, `password`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Auto increment value for astra_instance
-- ----------------------------
ALTER TABLE `astra_instance` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for users
-- ----------------------------
ALTER TABLE `users` AUTO_INCREMENT=1;
